import React, { Component } from 'react'
import Typist from 'react-typist';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
    AccordionItemState
} from 'react-accessible-accordion';

export default class Mobile extends Component {
    render() {
        return (
            <div className="home_bg sm-column">
                <div className="mobile">
                    <div className="mobile-title animated fadeInRight slow delay_1">
                        Разработка сайтов
                    </div>
                    <div className="mobile-description animated fadeInRight slow delay_2">
                        Предлагаем вам разработку сайтов под ключ для успешного развития бизнеса и получения новой прибыли.
                    </div>
                    <Accordion allowZeroExpanded={true}>
                        <AccordionItem className="mobile-carousel animated fadeInRight slow delay_3">
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                <div className="top">
                                    <div className="title">ЛЕНДИНГ ПЕЙДЖ</div>
                                    <div className="price">от 5 000 000 сум</div>
                                </div>
                                <div className="bottom">
                                    <div className="descr">
                                    Одностраничный сайт для вашего бизнеса. 
                                    Основной задачей Лендинга является репрезентацией информации Вашей компании.
                                    </div>
                                    <div className="arrow">
                                    <AccordionItemState>
                                        {({expanded}) => (
                                            <span className={expanded ? 'open' : 'closed'}>
                                                <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#B58F69"/>
                                                </svg>
                                            </span>
                                        )}
                                    </AccordionItemState>
                                    </div>
                                </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel >
                                • Уникальный дизайн <br/>
                                • Адаптация под мобильные и планшеты <br/>
                                • CMS (Система управления контентом) <br/>
                                • 500 Мб пространство <br/>
                                • Техническая поддержка (1 год) <br/>
                                • Контактные формы
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem className="mobile-carousel animated fadeInRight slow delay_3">
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                <div className="top">
                                    <div className="title">КОРПОРАТИВНЫЙ</div>
                                    <div className="price">от 8 000 000 сум</div>
                                </div>
                                <div className="bottom">
                                    <div className="descr">
                                        Содержит полную информацию о компании, о товарах и услугах, о владельце, о новых событиях в жизни компании.
                                    </div>
                                    <div className="arrow">
                                    <AccordionItemState>
                                        {({expanded}) => (
                                            <span className={expanded ? 'open' : 'closed'}>
                                                <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#B58F69"/>
                                                </svg>                                                
                                            </span>
                                        )}
                                    </AccordionItemState>
                                    </div>
                                </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel >
                                • Уникальный дизайн <br/>
                                • Адаптация под 4 устройства <br/>
                                • CMS (Система управления контентом) <br/>
                                • 1000 Мб пространство <br/>
                                • Корпоративная почта <br/>
                                • Новостной блок компании <br/>
                                • Техническая поддержка (1 год) <br/>
                                • Каталог продукции/услуги <br/>
                                • Галерея/Портфолио
                            </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem className="mobile-carousel animated fadeInRight slow delay_3">
                            <AccordionItemHeading>
                                <AccordionItemButton>
                                <div className="top">
                                    <div className="title">ЭКСКЛЮЗИВНЫЙ</div>
                                    <div className="price">Договорная цена</div>
                                </div>
                                <div className="bottom">
                                    <div className="descr">
                                    Разработка интернет-портала. 
                                    Мы готовы показать свой профессиональный подход к созданию сложных проектов.
                                    </div>
                                    <div className="arrow">
                                    <AccordionItemState>
                                        {({expanded}) => (
                                            <span className={expanded ? 'open' : 'closed'}>
                                                <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#B58F69"/>
                                                </svg>
                                            </span>
                                        )}
                                    </AccordionItemState>
                                    </div>
                                </div>
                                </AccordionItemButton>
                            </AccordionItemHeading>
                            <AccordionItemPanel >
                            • Уникальный дизайн <br/>
                            • Адаптация под 4 устройства <br/>
                            • CMS (Система управления контентом) <br/>
                            • Работа с большими данными <br/>
                            • Установка и настройка серверов <br/>
                            • Регистрация/Авторизация <br/>
                            • Уникальный код програмирования <br/>
                            • Оптимизация сайта <br/>
                            • Личный кабинет <br/>
                            • Техническая поддержка
                            </AccordionItemPanel>
                        </AccordionItem>                                                
                    </Accordion>
                </div>
                <div className="animation animated fadeIn slow">
                    <div className="animation__block">
                        <div className="animation__block--inner">
                            <div className="top">
                                <div className="left">
                                    HTML
                                </div>
                                <div className="right">
                                    <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#4C4C56"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="bottom">
                                <Typist speed={30} cursorClassName={`cursor`} className="code">
                                    <span className="orange">&lt;div</span> <span className="yellow">class</span><span>=</span><span className="green">"rect"</span><span className="orange">&gt;&lt;/div&gt;</span>
                                </Typist>                                
                            </div>
                        </div>
                        <div className="animation__block--inner">
                            <div className="top">
                                <div className="left">
                                    SCSS
                                </div>
                                <div className="right">
                                    <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#4C4C56"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="bottom">
                                <Typist speed={30} cursorClassName={`cursor`} className="code">
                                    <span className="yellow">.rect</span> <span>{`{`}</span>
                                    <br/><br/>
                                    <span className="purple">background</span><span>:</span> <span className="yellow">linear-gradient</span>(
                                    <br/><br/>
                                    <span className="orange">-119deg</span>,
                                    <br/><br/>
                                    <span className="yellow">$gray</span> <span className="orange">0%</span>,
                                    <br/><br/>
                                    <span className="yellow">$dark-gray</span> <span className="orange">100%</span>);}
                                </Typist>                                
                            </div>
                        </div>
                        <div className="animation__block--inner">
                            <div className="top">
                                <div className="left">
                                    JS
                                </div>
                                <div className="right">
                                    <svg width="22" height="13" viewBox="0 0 22 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.4429 11.7687L1.08394 2.38387C0.777488 2.07638 0.778003 1.57856 1.08553 1.27159C1.39301 0.964858 1.89111 0.965651 2.1978 1.27317L10.9997 10.0994L19.8015 1.27286C20.1083 0.965374 20.606 0.964581 20.9136 1.27127C21.0677 1.42515 21.1448 1.62675 21.1448 1.82834C21.1448 2.02942 21.0682 2.23022 20.9152 2.38383L11.5565 11.7687C11.4091 11.9168 11.2086 11.9999 10.9997 11.9999C10.7908 11.9999 10.5905 11.9166 10.4429 11.7687Z" fill="#4C4C56"/>
                                    </svg>
                                </div>
                            </div>
                            <div className="bottom">
                                <Typist speed={30} cursorClassName={`cursor`} className="code">
                                    <span className="yellow">var </span> 
                                    <span className="blue">colors</span> = <br/><br/> [<span className="green">"#74B087"</span>,<span className="green">"#DE7300"</span>,<span className="green">"#74B087"</span>];<br/><span></span><br/><span className="yellow">function</span> <span className="blue">animate</span>()
                                </Typist>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        )
    }
}
