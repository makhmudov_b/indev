import React, { Component } from 'react'
import Macbook from '../images/macbook.svg'
import SliderFirst from '../images/portfolio1.jpg'
import SliderSecond from '../images/portfolio2.jpg'
import SliderThird from '../images/portfolio3.jpg'
import SliderFourth from '../images/portfolio4.jpg'
import SliderFive from '../images/portfolio5.jpg'
import SliderSix from '../images/portfolio6.jpg'
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
import "../scss/slick.scss";
import Slider from "react-slick";

export default class Portfolio extends Component {
    constructor(){
        super();
        this.state = {
            activeSlide : 0
        }
    }
    render() {
        let settings = {
            dots: true,
            infinite: true,
            speed: 800,
            autoplay: true,
            pauseOnHover: true,
            autoplaySpeed: 2000,            
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:false,
            // beforeChange: (current, next) => this.setState({ activeSlide: next }),
            appendDots: dots => (
                <div>
                  <ul> {dots} </ul>
                </div>
              ),
              customPaging: i => (
                <div>
                  0{i + 1}
                </div>
              )            
          };
        return (
            <React.Fragment>
                <div className="home_bg pt-20 pb-20 animated fadeInRight delay_5 slow">
                    <div className="portfolio">
                    <h1 className="sm-show">Портфолио</h1>
                        <div className="portfolio__slider">
                            <Slider {...settings} className="item">
                                <div>
                                    <img src={SliderFirst} alt="portfolio" />
                                </div>
                                <div>
                                    <img src={SliderSecond} alt="portfolio" />
                                </div>
                                <div>
                                    <img src={SliderThird} alt="portfolio" />
                                </div>
                                <div>
                                    <img src={SliderFourth} alt="portfolio" />
                                </div>
                                <div>
                                    <img src={SliderFive} alt="portfolio" />
                                </div>
                                <div>
                                    <img src={SliderSix} alt="portfolio" />
                                </div>
                            </Slider>
                            <img src={Macbook} alt="macbook" />
                        </div>

                    </div>
                </div>
            </React.Fragment>            
        )
    }
}
